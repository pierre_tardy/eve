# This file has been automatically generated, DO NOT EDIT!

# Frozen requirements for "requirements/autoformat.in"

autoflake==0.7
autopep8==1.3.1
isort==4.2.5
pycodestyle==2.3.1
pyflakes==1.5.0
yapf==0.16.1

